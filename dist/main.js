window.onload = function () {
    freeFlow();
    hotfix1();
    hotfix2();
    bugfix1();
    feature1();
    feature2();
    feature3();
    feature4();
    feature5();
};

let branchOrder = [
    'master', 'uat', 'test', 'develop',
    'task1', 'task2',
    'hotfix/login_screen_logo', 'hotfix/dashboard_progress_bar',
    'bugfix/login_button_color',
    'feature/task1', 'feature/task2',
    'feature/task1_uat', 'feature/task2_uat',
    'feature/task1_test', 'feature/task2_test',
    'feature/task1_develop', 'feature/task2_develop',
];

let compareBranchesOrder = function(a, b) {
    return branchOrder.indexOf(a) - branchOrder.indexOf(b);
};

function getGitGraph(id) {
    let graphContainer = document.getElementById(id);
    return GitgraphJS.createGitgraph(graphContainer, {
        template: GitgraphJS.TemplateName.BlackArrow,
        compareBranchesOrder: compareBranchesOrder,
        author: 'Kumaravel <kumaravel@heptagon.in>',
    })
}

function freeFlow() {
    let gitgraph = getGitGraph('gc-free-flow');
    let master = gitgraph.branch('master');
    master.commit('Initial Commit').merge(
        master.branch('task1').commit('Added Task1')
    ).tag('v1.1.0').merge(
        master.branch('task2').commit('Added Task2')
    ).tag('v1.2.0');
}

function hotfix1() {
    let gitgraph = getGitGraph('gc-hotfix-1');
    let master = gitgraph.branch('master');
    master.commit('Initial Commit').merge(
        master.branch('hotfix/login_screen_logo').commit('Fixed the login screen logo position.')
    ).tag('v1.2.1').merge(
        master.branch('hotfix/dashboard_progress_bar').commit('Fixed the loader issue on the progress bar.')
    ).tag('v1.2.2');
}

function hotfix2() {
    let gitgraph = getGitGraph('gc-hotfix-2');
    let master = gitgraph.branch('master');
    let uat = master.branch('uat');
    master.commit('Initial Commit');
    let hotfix1 = master.branch('hotfix/login_screen_logo').commit('Fixed the login screen logo position.');
    uat.merge(hotfix1);
    master.merge(hotfix1).tag('v1.2.1');
    let hotfix2 = master.branch('hotfix/dashboard_progress_bar').commit('Fixed the loader issue on the progress bar.');
    uat.merge(hotfix2);
    hotfix2.commit('Fix added for firefox browser support.');
    uat.merge(hotfix2);
    master.merge(hotfix2).tag('v1.2.2');
}

function bugfix1() {
    let gitgraph = getGitGraph('gc-bugfix-1');
    let master = gitgraph.branch('master');
    let test = master.branch('test');
    let uat = master.branch('uat');
    master.commit('Initial Commit');
    let bugfix1 = master.branch('bugfix/login_button_color').commit('Updated the login button color to match the theme.');
    test.merge(bugfix1);
    bugfix1.commit('Fix added for the button border color.');
    test.merge(bugfix1);
    uat.merge(bugfix1);
    bugfix1.commit('Updated the color login button color.');
    test.merge(bugfix1);
    uat.merge(bugfix1);
    master.merge(bugfix1).tag('v1.2.3');
}

function feature1() {
    let gitgraph = getGitGraph('gc-feature-1');
    let master = gitgraph.branch('master');
    master.commit('Initial commit');
    let develop = master.branch('develop');
    let test = master.branch('test');
    let uat = master.branch('uat');
    let feature1  = master.branch('feature/task1');
    feature1.commit('Added new feature');
    develop.merge(feature1);
    test.merge(feature1);
    uat.merge(feature1);
    master.merge(feature1).tag('v1.3.0');
}

function feature2() {
    let gitgraph = getGitGraph('gc-feature-2');
    let master = gitgraph.branch('master');
    master.commit('Initial commit');
    let develop = master.branch('develop');
    let test = master.branch('test');
    let uat = master.branch('uat');
    let feature1  = master.branch('feature/task1');
    let feature2  = master.branch('feature/task2');
    feature1.commit('Added task 1 feature');
    feature2.commit('Added task 2 feature');
    develop.merge(feature1);
    test.merge(feature1);
    uat.merge(feature1);
    develop.merge(feature2);
    test.merge(feature2);
    master.merge(feature1).tag('v1.3.0');
    feature2.merge(master).commit('Additional changes on task 2 feature');
    uat.merge(feature2);
    master.merge(feature2).tag('v1.4.0');
}

function feature3() {
    let gitgraph = getGitGraph('gc-feature-3');
    let master = gitgraph.branch('master');
    master.commit('Initial commit');
    let develop = master.branch('develop');
    let feature1  = master.branch('feature/task1');
    let feature2  = master.branch('feature/task2');
    feature1.commit('Added task 1 feature');
    feature2.commit('Added task 2 feature');
    develop.merge(feature1);
    let feature2Develop = feature2.branch('feature/task2_develop');
    feature2Develop.merge(develop);
    develop.merge(feature2Develop);
}

function feature4() {
    let gitgraph = getGitGraph('gc-feature-4');
    let master = gitgraph.branch('master');
    master.commit('Initial commit');
    let develop = master.branch('develop');
    let feature1  = master.branch('feature/task1');
    let feature2  = master.branch('feature/task2');
    feature1.commit('Added task 1 feature');
    feature2.commit('Added task 2 feature');
    develop.merge(feature1);
    let feature2Develop = feature2.branch('feature/task2_develop');
    feature2Develop.merge(develop);
    develop.merge(feature2Develop);
    feature2.commit('Bug fix');
    feature2Develop.merge(feature2);
    develop.merge(feature2Develop);
}

function feature5() {
    let gitgraph = getGitGraph('gc-feature-5');
    let master = gitgraph.branch('master');
    master.commit('Initial commit');
    let develop = master.branch('develop');
    let test = master.branch('test');
    let feature1  = master.branch('feature/task1');
    let feature2  = master.branch('feature/task2');
    feature1.commit('Added task 1 feature');
    feature2.commit('Added task 2 feature');
    develop.merge(feature1);
    let feature2Develop = feature2.branch('feature/task2_develop');
    feature2Develop.merge(develop);
    develop.merge(feature2Develop);
    feature2.commit('Bug fix');
    feature2Develop.merge(feature2);
    develop.merge(feature2Develop);
    test.merge(feature1);
    let feature2Test = feature2.branch('feature/task2_test');
    feature2Test.merge(test);
    test.merge(feature2Test);
    feature2.commit('Bug fix');
    feature2Test.merge(feature2);
    test.merge(feature2Test);
}
